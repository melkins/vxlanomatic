// from <net/vxlan.h>

#define IANA_VXLAN_UDP_PORT     4789

struct vxlanhdr {
	__be32 vx_flags;
	__be32 vx_vni;
};
