vxlanomatic
===========
This is a proof of concept of using a BPF cpumap to implement VNI-based RSS as described in
https://developers.redhat.com/blog/2021/05/13/receive-side-scaling-rss-with-ebpf-and-cpumap#practical_use_case__an_issue_on_low_end_hardware

The motivation for this is that in Merge testbeds we are using Mellanox
ConnectX-6 cards, which don't seem to support using the VXLAN VNI as input to
the RSS hash algorithm, to spread out packets over all hardware rx queues.
Instead, it hashes on the UDP source/dest ports. The UDP source port is
generally derived from the inner source/dest IP addresses.

In virtualized experiments we have the possibility that a hypervisor machine is
multiply connected to an emulation server, and what ends up happening is that
the same UDP source port is used, so traffic to a particular hardware queue on
the emulation server's NIC is amplified to the point where the CPU core servicing
interrupts can't handle the load.

Thus, in order to get better performance, it is beneficial to spread the load
over all the available cores on the emulation server. This XDP program inspects
the VXLAN VNI, and then punts the processing of the packet to another CPU core.
