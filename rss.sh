#!/usr/bin/env bash

# Example script to set up VNI-based RSS on emu001

rm -f /sys/fs/bpf/cpu_map
rm -f /sys/fs/bpf/vxlan_rss
bpftool net detach xdp dev ens1np0
bpftool net detach xdp dev ens2np0

bpftool map create /sys/fs/bpf/cpu_map type cpumap key 4 value 8 entries 48 name cpu_map

# fill the cpu map, qsize=256 (0x0 0x1 0x0 0x0)
for i in 0 1 2 3 4 5 6 7 8 9 a b c d e f 10 11 12 13 14 15 16 17 18 19 1a 1b 1c 1d 1e 1f 20 21 22 23 24 25 26 27 28 29 2a 2b 2c 2d 2e 2f; do
        bpftool map  update pinned /sys/fs/bpf/cpu_map key hex $i 0 0 0 value hex 00 01 0 0 0 0 0 0
done

bpftool prog load vxlan_rss.o /sys/fs/bpf/vxlan_rss type xdp map name cpu_map name cpu_map

# limitation of the current kernel in FC35, otherwise we can't load the xdp prog
ip link set dev ens1np0 mtu 3498
ip link set dev ens2np0 mtu 3498
# the bond needs to match, otherwise BGP gets unhappy
ip link set dev xleaf64 mtu 3498

bpftool net attach xdp pinned /sys/fs/bpf/vxlan_rss dev ens1np0
bpftool net attach xdp pinned /sys/fs/bpf/vxlan_rss dev ens2np0
