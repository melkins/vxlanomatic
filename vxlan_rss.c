// SPDX-License-Identifier: GPL-2.0

#include <linux/bpf.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <linux/in.h>
#include "vxlan.h"

// for now just redistribute across all cores without concern as to which numa node is associated with the NIC
#define NUM_CPUS 48

typedef __u8 u8;
typedef __u16 u16;
typedef __u32 u32;

/* Special map type that can XDP_REDIRECT frames to another CPU */
struct {
	__uint(type, BPF_MAP_TYPE_CPUMAP);
	__uint(key_size, sizeof(u32));
	__uint(value_size, sizeof(struct bpf_cpumap_val));
} cpu_map SEC(".maps");

SEC("xdp")
int vxlan_rss(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *data = (void *)(long)ctx->data;
	struct ethhdr *eth;
	struct udphdr *udph;
	struct iphdr *iph;
	struct vxlanhdr *vxh;
	void *nh;
	u32 vni;
	u32 cpu_dest;

	/* data in context points to ethernet header */
	eth = data;

	/* set pointer to header after ethernet header */
	nh = data + sizeof(*eth);
	if (nh > data_end)
		return XDP_DROP; // malformed packet

	if (eth->h_proto != bpf_htons(ETH_P_IP))
		return XDP_PASS; // we are looking for IPv4 packets only

	iph = nh;
	nh = nh + sizeof(*iph);
	if (nh + 1 > data_end)
		return XDP_DROP; // malformed packet
	if (iph->protocol != IPPROTO_UDP)
		return XDP_PASS; // we are looking for UDP packets only

	udph = nh;
	nh = nh + sizeof(*udph);
	if (nh + 1 > data_end)
		return XDP_DROP; // malformed packet
	if (udph->dest != bpf_htons(IANA_VXLAN_UDP_PORT))
		return XDP_PASS; // we are looking for VXLAN packets only

	vxh = nh;
	nh = nh + sizeof(*vxh);
	if (nh + 1 > data_end)
		return XDP_DROP; // malformed packet

	vni = bpf_ntohl(vxh->vx_vni) >> 8;

	// distribute the streams by the VXLAN VNI.
	// In Merge, there is a separate VNI for each node<-->emu connection
	cpu_dest = vni % NUM_CPUS;

#if DEBUG
	bpf_printk("VNI %u -> CPU%u\n", vni, cpu_dest);

	long ret = bpf_redirect_map(&cpu_map, cpu_dest, 0);
	if (ret != XDP_REDIRECT)
		bpf_printk("redirect failed: %ld\n", ret);
	return ret;
#else
	return bpf_redirect_map(&cpu_map, cpu_dest, 0);
#endif
}

char _license[] SEC("license") = "GPL";
