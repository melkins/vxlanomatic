CLANG=clang -O2 -target bpf -emit-llvm -c -g
LLC=llc -march=bpf -filetype=obj
CFLAGS= #-DDEBUG=1

all: vxlan_rss.o

%.o: %.c
	$(CLANG) $(CFLAGS) $<
	$(LLC) -o $@ $*.bc

clean:
	rm -f *.o *.bc
